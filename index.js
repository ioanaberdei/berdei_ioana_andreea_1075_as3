const FIRST_NAME = "Ioana-Andreea";
const LAST_NAME = "Berdei";
const GRUPA = "1075";

/**
 * Make the implementation here
 */
class Employee {
  constructor(name, surname, salary) {
    this.name = name;
    this.surname = surname;
    this.salary = salary;
  }

  getDetails() {
    return `${this.name} ${this.surname} ${this.salary}`;
  }
}

class SoftwareEngineer extends Employee {
  constructor(name, surname, salary, experience) {
    super(name, surname, salary);
    if (!experience) this.experience = "JUNIOR";
    else this.experience = experience;
  }

  applyBonus() {
    if (this.experience === "JUNIOR") return this.salary + this.salary * 0.1;
    else if (this.experience === "MIDDLE")
      return this.salary + this.salary * 0.15;
    else if (this.experience === "SENIOR")
      return this.salary + this.salary * 0.2;
    else return this.salary + this.salary * 0.1;
  }
}

module.exports = {
  FIRST_NAME,
  LAST_NAME,
  GRUPA,
  Employee,
  SoftwareEngineer
};
